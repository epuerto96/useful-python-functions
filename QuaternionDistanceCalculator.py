from math import *

def distanceCalculator(thetaX, thetaY, distanceA, distanceB):
    radianX = radians(thetaX)                               
    radianY = radians(thetaY)
    finalRadian = 2 * (acos(cos(radianX / 2) * (cos(radianY/2))))
    distance = sqrt(distanceA**2 + distanceB**2 - 2*distanceA*distanceB*(cos(finalRadian))) # Law of cosines solving for C
    return distance     


if __name__ == "__main__":
    print(distanceCalculator(45,45,35,17))

    
    
