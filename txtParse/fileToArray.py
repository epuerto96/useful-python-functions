import numpy as np
import os
import sys

os.chdir(os.path.dirname(os.path.abspath(__file__))) #This moves the working directory to the location of the .py file


def fileToArray(filename):                           #Takes in a file, trys to open it and populate an array. If it fails posts error and ends code execution
    try:
        array = np.loadtxt(filename, dtype=str)
        return array
    except OSError:
        print("{} does not exist in {}\\".format(filename, os.getcwd() ))   
        sys.exit(1)

if __name__ == "__main__":
    data = fileToArray("data.txt") 

