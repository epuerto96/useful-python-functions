
def simpleProduct(values):
    solution = values[0] * values[1]
    print("The Product of {} and {} is : {}".format(values[0], values[1], solution))

def userPrompt():
    x = float(input("Enter the Multiplicand: "))
    y = float(input("Enter the Multiplier: "))  
    return x , y

if __name__ == "__main__":
    while True:
        simpleProduct(userPrompt())
        